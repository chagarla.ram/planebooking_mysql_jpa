package org.flightBooking.Services;
import org.flightBooking.Models.ScheduledFlights;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BusinessCalculator implements CalculatePrice {

    @Override
    public double calculatePrice(List<ScheduledFlights> scheduledFlightTravelClass, int passengers, String flyDate) {
        double weekbaseincre = 1;
        try {
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
            SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd");
            Date tmpflyDate = formatter.parse(flyDate);
            String week = simpleDateformat.format(tmpflyDate);
            if (week.equals("Monday") || week.equals("Friday") || week.equals("Sunday")){
                weekbaseincre = 1.4;
                return weekbaseincre;
            }
            else
                return weekbaseincre;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return weekbaseincre;

    }
}