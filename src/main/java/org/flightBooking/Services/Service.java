package org.flightBooking.Services;

import org.flightBooking.Models.ScheduledFlights;
import org.flightBooking.Models.Flight;
import org.flightBooking.Models.ScheduledFlightsTravelClass;
import org.flightBooking.Repository.FlightRepository;
import org.flightBooking.Repository.ScheduledFlightsRepository;
import org.flightBooking.Repository.ScheduledTravelClassRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@org.springframework.stereotype.Service
public class Service {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private ScheduledTravelClassRepository scheFlightsClassRepository;

    @Autowired
    private ScheduledFlightsRepository scheduledFlightsRepository;

    public String[] searchFlight(String source, String destination, int noOfPassengers, String flyDate, String selectedClass) {
        double weekbaseincre = 1, firstincrebase = 1, increbase = 1, seatPrice = 0;int i=0;
        //getting related flights and scheduled flights
        List<ScheduledFlights> scheduledFlightsAll = scheduledFlightsRepository.findBySourceAndDestination(source,destination);
          //check avaliable flight for that travel class
        if (scheduledFlightsAll.isEmpty()) {
            String[] sresult = new String[2];
            sresult[0] = "No Flights are avaliable for ";
            sresult[1] = "";
            return sresult;
        }
        else{
           int noofscheduledflights = scheduledFlightsAll.size();
           int[][] schflight_id = new int[noofscheduledflights][3];
           String[] flight_name = new String[noofscheduledflights];

         for (ScheduledFlights p : scheduledFlightsAll) {
           schflight_id[i][0] = p.getFlightId();
           schflight_id[i][1] = p.getSchedflightId();
           flight_name[i] = flightRepository.findFlightName(p.getSchedflightId());
           schflight_id[i][2]= scheFlightsClassRepository.findPriceForAvaliable(p.getSchedflightId(), selectedClass, noOfPassengers);
           i++;
         }

          CalculatePrice calculatePrice;
          switch (selectedClass) { // switch case for the selected seat class and fare base on the class selected
            case "Economy":  // Calculating the % increase in the economy fare based on the range current booking falls with total available tickets
             calculatePrice = new EconomyCalculator();
                        increbase = calculatePrice.calculatePrice(scheduledFlightsAll, noOfPassengers, flyDate);
                        break;

                    case "BusinessClass": // Calculating the % increase in the Business Class fare based on the week days. More fare for Monday, Firday and Saturday
                        calculatePrice = new BusinessCalculator();
                        weekbaseincre = calculatePrice.calculatePrice(scheduledFlightsAll, noOfPassengers, flyDate);
                        break;

                    case "FirstClass":  // Calculating the % increase in the First Class fare by compare the departure date with the current booking date
                        calculatePrice = new FirstClassCalculator();
                        firstincrebase = calculatePrice.calculatePrice(scheduledFlightsAll, noOfPassengers, flyDate);
                        break;
                }
                //calculating the base fore no of passengers
//                sresult[0] = flight_name;
//                sresult[1] = Double.toString(increbase * seatPrice * weekbaseincre * firstincrebase * noOfPassengers);
            }

        return new String[0];
    }
   }


