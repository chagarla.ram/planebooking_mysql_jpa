package org.flightBooking.Services;
import org.flightBooking.Models.ScheduledFlights;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EconomyCalculator implements CalculatePrice {

    @Override
    public double calculatePrice(List<ScheduledFlights> scheduledFlightTravelClass, int passengers, String flyDate) {
        double increbase = 1;
        int currentbookingno =1, seatsAvaliableineconomy=1;
      //  int totalnoofseatsA = scheduledFlightTravelClass.get(0).getAvaliableBusinessclassSeats();
        if (currentbookingno <= ((40 * seatsAvaliableineconomy) / 100)) {
            increbase = 1;
        } else if ((currentbookingno <= ((90 * seatsAvaliableineconomy) / 100)) && (currentbookingno >= ((40 * seatsAvaliableineconomy) / 100))) {
            increbase = 1.3;
        } else {
            increbase = 1.6;
        }
        return increbase;
    }
}
