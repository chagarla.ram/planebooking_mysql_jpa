package org.flightBooking.Services;
import org.flightBooking.Models.ScheduledFlights;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

class FirstClassCalculator implements CalculatePrice {

    @Override
    public double calculatePrice(List<ScheduledFlights> scheduledFlightTravelClass, int passengers, String flyDate) {
        Date nowdate = new Date();
        double firstincrebase = 1;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd");
            Date tmpflyDate = formatter.parse(flyDate);
            // Date nowtmpdate =formatter.parse(new Date());
            if (tmpflyDate.compareTo(new Date()) < 1) {
                firstincrebase = firstincrebase + 1;
            } else {
                double datedifference = ((tmpflyDate.getTime() - nowdate.getTime()) / (24 * 60 * 60 * 1000)) + 1;
                if (datedifference >= 1 && datedifference < 10)
                    firstincrebase = firstincrebase + ((10 - datedifference) / 10);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return firstincrebase;

    }
}

