package org.flightBooking.Services;

import org.flightBooking.Models.ScheduledFlights;

import java.util.Date;
import java.util.List;

public interface CalculatePrice {
    double calculatePrice(List<ScheduledFlights> scheduledFlightTravelClass,int passengers, String flyDate);
}
